# Motorwork
`motorwork` is just a shell script to install dependences needed to build tools
needed to work on [motor](https://github.com/0x1A/motor).

## What it does
`motorwork` checks for dependencies installed on the system (Currently supports Debian, Ubuntu, and Arch) and
installs needed dependencies. After dependencies are installed it pulls the latest
stable needed libraries, compiles, and installs them.

### Current Dependencies (Installed through native package managers)
Debian:
* `curl`
* `build-essential`
* `build-deps` for `libsdl1.2 sdl-image1.2 sdl-mixer1.2 sdl-ttf2.0`

Arch has dependencies solved through `base-devel`.

### Dependencies pulled and built from source
* [SDL2 2.0.1](http://libsdl.org/download-2.0.php)
* [SDL2-image 2.0.0](http://www.libsdl.org/projects/SDL_image/)
* [SDL2-ttf](http://www.libsdl.org/projects/SDL_ttf/)
* [SDL2-mixer 2.0.0](http://www.libsdl.org/projects/SDL_mixer/)
* [libyaml 0.1.4](http://pyyaml.org/wiki/LibYAML)

# Clean
`clean` is a shell script that cleans up the compile of Motor after running `make`
