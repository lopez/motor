// Global timer set's SPRITE ANIMATION FRAMES and the WINDOW FRAME.
// Not hardware accelerated, no vsync (ex to gl) [cpu calculated]

#ifndef GLOBTIMER
#define GLOBTIMER

#include "mains.h"

//Gobal timer class
class GlobalTimer
{
    private:
        int StartTick;
        int PausedTick;

        bool Started;
        bool Paused;

    public:
        GlobalTimer();
        ~GlobalTimer();
        void StartTimer();
        void StopTimer();
        void PauseTimer();
        void UnpauseTimer();

        int Get_Ticks();

        bool is_started();
        bool is_paused();
};

GlobalTimer::GlobalTimer()
{
    StartTick - 0;
    PausedTick = 0;
    Started = false;
    Paused = false;
    StartTimer();
}

GlobalTimer::~GlobalTimer()
{
    StopTimer();
}

void GlobalTimer::StartTimer()
{
    Started = true;
    Paused = false;

    StartTick = SDL_GetTicks();
}

void GlobalTimer::StopTimer()
{
    Started = false;
    Paused = false;
}

void GlobalTimer::PauseTimer()
{
    if ((Started == true) && (Paused == false)) {
        Paused = true;
        PausedTick = SDL_GetTicks() - StartTick;
    }
}

void GlobalTimer::UnpauseTimer()
{
    if (Paused == true) {
        Paused = false;
        StartTick = SDL_GetTicks() - PausedTick;
        PausedTick = 0;
    }
}

int GlobalTimer::Get_Ticks()
{
    if (Started = true) {
        if (Paused = true) {
            return PausedTick;
        }
        else {
            return SDL_GetTicks() - StartTick;
        }
    }
    return 0;
}

bool GlobalTimer::is_started()
{
    return Started;
}

bool GlobalTimer::is_paused()
{
    return Paused;
}

#endif
