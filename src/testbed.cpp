#include "mains.h"
#include "motor.h"
#include "input.h"
#include "filesystem.h"

int main (int args, char *argc[])
{
    WriteToLog();
    Init SYSINIT;

    bool quit = false;

    CreateWindow();
    Player TestPlayer;
    Entity TestEnt1;
    Entity TestEnt2;
    TestPlayer.LoadSprite(1,"res/testplayersprite_dleft.png");
    TestPlayer.SetRects(75,45,0,0);
    TestPlayer.ShowMove();
    TestPlayer.DrawSprite();
    TestEnt1.LoadSprite(2,"res/testplayersprite_dright.png");
    TestEnt1.SetRects(75,45,100,0);
    TestEnt1.DrawSprite();
    TestEnt2.LoadSprite(3,"res/testplayersprite_down.png");
    TestEnt2.SetRects(75,45,100,100);
    TestEnt2.DrawSprite();

    SetParseFile("public.yaml");

    GlobalTimer fps;

    while(quit == false) {
        fps.StartTimer();
        while(PollEvent()) {
            if ((MainEvent.type == SDL_KEYDOWN) && (MainEvent.key.keysym.sym == SDLK_ESCAPE)) {
                quit = true;
                if (quit == true) {
                    DestRenderer();
                    DestWindow();
                }
            }
        }
    }
    return 0;
}
