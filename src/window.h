//TODO:
//1. Rewrite old window.h
//2. Needs to detect current desktop resolution to launch at full screen
//3. UI for res settings
//4. Fix debug
//5. Maybe change Window(), too long

#ifndef WINDOW
#define WINDOW

#include "mains.h"
#include "motor.h"

int w_height;
int w_width;
bool Windowed;
bool WindowErrorCheck;
bool WindowError();
void Debug();
void ToggleFullScreen();
void DestWindow();
void DestRenderer();
void NumDisplay();
SDL_Window *window;
SDL_Renderer *w_renderer;
Uint32 w_windowflag;
Uint32 WindowFlags();

SDL_Renderer *GetRenderer() {return w_renderer;}

static int CreateWindow()
{
    SDL_GetNumVideoDisplays();
    SDL_GetWindowDisplayIndex(window);

    window = SDL_CreateWindow(
            "Motor Test",
            SDL_WINDOWPOS_UNDEFINED,
            SDL_WINDOWPOS_UNDEFINED,
            1280,
            720,
            SDL_WINDOW_SHOWN);

    w_renderer = SDL_CreateRenderer (window, 0, SDL_RENDERER_ACCELERATED);
    std::cout << "Window created\n";
    NumDisplay();
    return 0;
}

void NumDisplay()
{
    SDL_DisplayMode current;
    int i;
    for (i = 0; i < SDL_GetNumVideoDisplays(); i++) {
        int a = SDL_GetCurrentDisplayMode(i, &current);

        if (a != 0) {
            std::cout << i << SDL_GetError();
        }
        else {
            std::cout << i
                << current.w << " x "
                << current.h << " @ "
                << current.refresh_rate << " hz\n";
            }
        }
}

void DestRenderer()
{
    std::cout << "Renderer destroyed\n";
    SDL_DestroyRenderer(w_renderer);
}

void DestWindow()
{
    std::cout << "Window destroyed\n";
    SDL_DestroyWindow(window);
}

#endif
